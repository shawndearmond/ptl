# Praise The Lord - Digital Song Repository

## Introduction

This is the digital file repository for Praise The Lord (aka Suzy's Opus).

The purpose of this project is to digitize all songs into a standard format that so they can be easily shared among the [Gerard Family Camp](http://www.gerardfamilycamp.org) (GFC) Music Group.

## About Suzy Gazley

Suzy Gazley was the music leader for GFC for over thirty years. She is a retired science school teacher, and spent much of her free time collecting Christian worship music to use when she led music GFC, as well as for [St. James Cathedral](https://stjamesfresno.org) in Fresno, California.

Suzy called her collection "Praise The Lord", or PTL for short, and organized it in a hand-written lyric book and chord sheets. The most current version is a single large three-ring binder, lovingly referred to as Suzy's Opus. Her work includes over 150 pages of songs, stored in 8 milk crates.

## PTL Digitization Project

The app to use is [OnSong](https://onsongapp.com), available for [iOS](https://itunes.apple.com/us/app/onsong/id502344938) and [macOS](https://itunes.apple.com/us/app/onsong-console/id1059830141). This app is full-featured, and supports a variety of import and export formats. Most notably for this project, the app utilizes the [ChordPro format](http://www.chordpro.org/) which is an easy-to-use text-based format for adding chords to songs. 

[This shared file folder on Drop Box](https://www.dropbox.com/sh/ng0miucp9zz9ed9/AAAeLJy4ztGYtaHfUdyxDizta?dl=0) is used by the project team for easy access to the songs.

### Entering Song Chords and Metadata

With some creative web searching, you can find many songs with chords. Enable the "Add to OnSong" Safari extension and use it to import songs. 

It's important to match the key in PTL, so if the song you find is in a different key, you can use the "Rewrite Into Key" function in OnSong, which is the piano icon on the song edit screen.

Tips for entering chords:

* If the chord is played on the first syllable of a word, the chord goes at the beginning of the word:
  `[C]Seek ye [G]first the [Am]kingdom of [Em]God`
* If the chord is played after the word, perhaps after a whole line, add a space and then the chord:
  `[F]And His [C]righteous[G]ness [G7]`
* If the chord is played on a middle syllable, the chord goes in the middle of the word, before the appropriate syllable, with no spaces:
  `[F]Alle[C]lu, alle[G]lu[C]ia`
* If a word is very drawn out with many chords, adding one or more dashes can help readability:
  `[C]Al-[G]le-[Am]lu-[Em]ia`
* If the chord is played before the word, add the chord, then two spaces, then the word:
  `[G]  You are my [D]strength when I am [Em]weak`
* It may be frustrating to move chords, so sometimes it's easier to use the "Chords Over Lyrics" chord format (in the hammer menu on the song edit screen). Just remember to switch it back when you're done.

Be sure to complete the following metadata for each song, in the (i) section on the song edit screen.

* Song title (without the page number)
* Copyright information (Available for most songs in /Resources/Copyright.pdf file. If the song is in the public domain, enter "public domain")
* Book: Praise The Lord
* Number: Page number in PTL (without leading zeros)

If available, the following metadata would be nice to have:

* Artist
* Key
* Flow
* [CCLI](https://us.search.ccli.com) Song #
* Book: Hymnal (if the song is also in the Hymnal)

### Exporting Song Files

Once a song is complete, export it from OnSong in the native ".onsong" format into the "OnSong Files" folder.

### More notes

You may see a ".git" folder, or a ".gitignore" file. Please ignore them. And please don't delete them.
